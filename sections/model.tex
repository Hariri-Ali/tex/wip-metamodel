\section{The Model}\label{sec:model}
In this section, we describe the proposed model and how it enables finer-grained and configurable authorisation lifecycless.

\subsection{Model Components}\label{sec:components}
The model consists of six elements and two functions as shown in Figure ~\ref{fig:model}.
The evaluation function evaluates decision predicates, which rely on attributes and the current lifecycle phase, then it outputs an authorisation decision and directives.
The transition function takes the evaluation outcome, attributes, and the current phase as inputs, and evaluates lifecycle controllers to determine the next phase.
These components compose authorisations and drive their lifecycles.
They are further described in this section.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.99\linewidth]{figures/model.png}
	\caption{Components of the proposed model}
	\label{fig:model}
\end{figure}

\subsubsection{Attributes}\label{sec:attributes}
Attributes are characteristics about the subject, object or environment, such as a role, file type or temperature.
We assume that all attributes are mutable and may change during the authorisation.
A change may occur as a consequence of the authorisation process itself, or due to administrative actions or environmental changes.
For instance, an increase in temperature is an environmental change, while updating the \texttt{last\_modified} date of a file may be a consequence of an authorisation that permitted writing to the file.
When an attribute value changes, a re-evaluation of decision predicates must be triggered.

\subsubsection{Lifecycle Phases}\label{sec:phases}
We consider all authorisations to have a lifecycle as described in Section ~\ref{sec:lifecycle}.
Thus, lifecycle phases are a core component of the model, defined and described in Section ~\ref{sec:lcphases}.

\subsubsection{Decision Predicates}\label{sec:predicates}
Decision predicates constitute security policies.
They are logical predicates over attributes, and are evaluated against a request to determine whether an authorisation can be granted, given the values of the attributes of interest.
Decision predicates must be classified according to lifecycle phases, as described in Section \ref{sec:classification}, such that they are evaluated only when the authorisation lifecycle is in the phase under which they are classified.
Taking data flow as an example, decision predicates that are classified as \(retention\) predicates, must only be evaluated when the lifecycle is in the \(retention\) phase (i.e., data is being stored).
Decision predicates must be re-evaluated whenever an attribute value changes, and the authorisation decision must be updated accordingly.
Moreover, when the authorisation lifecycle transitions to a new phase, decision predicates classified under the new phase must be evaluated even if no attribute updates occurred.

\subsubsection{Decisions}\label{sec:decisions}
Decisions refer to the evaluation outcome of decision predicates, which determines whether authorisations can be granted or not.
Our model extends the classical two-valued decisions (i.e., \(permit\), \(deny\)) to n-valued decisions to cover additional values such as \(error\), \(admissible\) or \(not\) \(admissible\).

\subsubsection{Directives}\label{sec:directives}
Directives are mandatory or optional actions that need to be performed as part of the authorisation, including subject obligations, attribute updates, and other procedures defined by policies.
For example, a directive may specify that a smart lock must be unlocked if the authorisation to enter a building is granted.
This allows taking proactive actions as part of the authorisation.

\subsubsection{Lifecycle Controllers}\label{sec:controllers}
Lifecycle controllers are rules that drive the authorisation lifecycle and define transitions between phases.
They are if-else statements that determine the next phase of the lifecycle based on its current phase, the authorisation decision and other conditions (e.g., about attributes or directives).
As such, lifecycle controllers must be able to capture contextual information and drive the lifecycle accordingly.
A pseudo-code example of a lifecycle controller is shown in Listing ~\ref{lst:controller}
Lifecycle controllers must be specified by the system administrator only, and policy authors must classify their policies based on the phases defined in the lifecycle controllers.
Thus, administrators configure the enforcement system with a set of authorisation lifecycles each targeting a specific technology or use-case, while policy authors use these lifecycles by defining policies for each of the corresponding phases.

\begin{lstlisting}[float=!tbh,caption={Lifecycle expressed as if-else statements},label={lst:controller}]
	If currentPhase is ''phaseA'' and authDecision is ''permit'' and directives are enforced
	  Then change phase to ''phaseB''
	Else If currentPhase is ''phaseA'' and authDecision is ''permit''
	  Then change phase to ''phaseC''
	Else change phase to ''exit''
\end{lstlisting}

\subsubsection{Evaluation Function}\label{sec:evalfunc}
The evaluation function is the component that makes authorisation decisions (e.g., \(permit\), \(deny\)), and determines directives.
It takes the current lifecycle phase as input, then evaluates decision predicates that are classified under the current phase by retrieving and evaluating attributes.
The output of the evaluation outcome is an authorisation decision combined with a set of directives.
The evaluation function must be invoked whenever the phase changes or an attribute value is updated.

\subsubsection{Transition Function}\label{sec:tranfunc}
The transition function is the component that drives the lifecycle and changes the phase.
It takes the current phase as input, then evaluates lifecycle controllers to determine the next phase in the lifecycle.
Lifecycle controllers may include conditions about attributes or about the outcome of the evaluation function, so attributes, decision and directives are taken as input in the transition function.
The transition function must be invoked right after every evaluation by the evaluation function.the
The transition function may also be invoked upon attribute updates, if the lifecycle relies on attributes and needs to react to changes.

\subsection{Evaluation Flow}\label{sec:flow}
When an authorisation request is created, the evaluation starts by calling the transition function to select the appropriate lifecycle.
The transition function evaluates lifecycle controllers and sets the first phase in the lifecycle.
Thereafter, the evaluation function is called to evaluate decision predicates classified under the selected phase, and evaluation outcome is enforced.
The transition function is then invoked again to determine determine whether the lifecycle must transition to another phase or remain in the same phase.
If the phase does not change, the system waits for attribute updates or user input (e.g., end authorisation), upon which the evaluation function is invoked again.
Otherwise, decision predicates classified under the new phase are evaluated and the new decision and directives are enforced.
The evaluation of decision predicates is again followed by calling the transition function and so on until the end of the authorisation.
To sum up, every time a transition to a different phase occurs, the evaluation function will be invoked and the authorisation decision will be updated, then the transition function will be called again to check whether the phase must change.
As an example, Figure \ref{fig:evalflow} shows a flow chart describing the evaluation flow of a \ac{ucon} authorisation using our model.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.95\linewidth]{figures/evalflow.png}
	\caption{Evaluation flow of a \ac{ucon} authorisation}
	\label{fig:evalflow}
\end{figure}


\subsection{Authorisation Process}\label{sec:authprocess}
We use the term \textit{Authorisation Process} to refer to all events, evaluations and other elements belonging to the lifetime of a single authorisation.
An authorisation process is initiated by an \textit{Authorisation Request}, and ends when its lifecycle reaches an exit phase (e.g., lifecycle of safety-critical use-cases specifies that the authorisation process ends upon completion of safety measures not upon revocation).
Since the authorisation processes start with a request, each authorisation process is associated with, and uniquely identified by, the request that started it.
An authorisation request is a query to check whether a \textit{specific subject} is allowed to exercise a \textit{specific right} on a \textit{specific object}.
Thus, it can be defined by the tuple \(Rq := (s,o,r)\), where \(s\) is the subject, \(o\) is the object, and \(r\) is the access right.
While the focus of this paper is on authorisations only, our objective is to cover policy-based systems that incorporate other lifecycles, such as the credential exchange system  we introduced in ~\cite{siuv}.
The request in such systems is not a query about a \textit{subject} exercising a \textit{right} on an \textit{object}, but rather a query about the capabilities that a specific subject has within a specific system and context.
For this reason, we define the request as an assignment to a subset of attributes, which can be subject, object and/or environmental attributes.
Let \(A\) be the set of all attributes and \(V\) be the set of all possible values for \(A\).
Also, let \(A_R\) be the subset of attributes that are present in the request such that \(A_R \subset A\).
We define the request as \(Rq: A_R \rightarrow V\) a partial mapping from \(A_R\) to \(V\) representing the assignment of values to the request attributes.