\section{Background and Related Work}\label{sec:background}
Park and Sandhu ~\cite{ucon} introduced \ac{ucon} to extend access control with mutability of attributes and continuity of control.
Unlike preceding models, \ac{ucon} assumes that attribute values may change during usage of resources, so it specifies that policies must be continuously evaluated while access is in progress.
\ac{ucon} classifies decision predicates as \(pre\) and \(ongoing\) such that:
\(pre\) predicates are evaluated when an access request is made to decide whether to grant or deny access, while
\(ongoing\) predicates are re-evaluated whenever an attribute value changes while access is in progress.
\ac{ucon} also incorporates obligations, which are mandatory actions that must be performed by the subject to gain authorisation, and they are also classified as \(pre\) and \(ongoing\) obligations.
Moreover, \ac{ucon} adds optional procedures defined in security policies to update attribute values.
Attribute updates are classified as \(pre\), \(ongoing\) and \(post\) updates such that \(pre\) updates are enacted before granting access, \(ongoing\) updates are enacted while access is in progress, and \(post\) updates are performed after the end of access.

\ac{ucon} revokes access when the security policy is no longer satisfied, which may disrupt user interactions in highly dynamic environments such as pervasive computing.
For this reason, Almutairi and Siewe ~\cite{almutairi2011ucon} extended \ac{ucon} with the ability to adapt to changing contexts rather than revoking authorisation altogether.
They added a phase designated as ``adapting'' to reflect the stage at which the authorisation session adapts to a changing context.
They also specify their model as a \ac{dfa} that describes how an access request is handled.
The \ac{dfa}, however, is coarse-grained and cannot be changed to support use-cases that require a different \ac{dfa}.

Similarly, Dimitrakos et al. ~\cite{ucs2020plus} extended \ac{ucon} with trust awareness by involving trust level evaluation in authorisation decisions.
Their model allows subjects to improve their trust level instead of denying authorisation if the trust level is low.
This was achieved by adding a counter of re-evaluations such that the authorisation remains on hold until the subject improves their trust or until the re-evaluation counter expires.
While this does add continuity to all phases, it remains restrictive to their use-case and the three traditional phases of \ac{ucon}.
Moreover, their model cannot capture the context of re-evaluations, so the counter may expire due to re-evaluations unrelated to trust level, and consequently not allowing the subject to improve their trust.

In ~\cite{conti2012crepe}, Conti et al. introduced a context-aware access control model for Android devices.
Their model monitors contextual information and reacts to changes by revoking a running application or service.
While their model is not based on \ac{ucon}, it provides similar novelties.
The model has similar limitations to \ac{ucon} as it does not cover monitoring before granting and after denying access, and it does not include a fine-grained authorisation lifecycle.

Bandopadhyay et al. ~\cite{datapal} leveraged \ac{ucon} to handle data privacy throughout the data lifecycle.
Their framework allows users to intervene and revoke the consent to use their data.
Although data lifecycle consists of five stages, they used \ac{ucon}'s three phases only.
This limits the expressibility of the model and the ability to capture specific contexts related to the data lifecycle stages.

Likewise, other works ~\cite{katt2008general,cirillo2020intentkeeper,ucs2016cloudsys,ucs2017iot,ucs2017mqtt,bigucon} have also built on \ac{ucon} to enable continuous authorisation.
Nonetheless, they remain restrictive to their corresponding use-cases, and limited to \ac{ucon}'s coarse-grained three phases only (i.e., \(pre\), \(ongoing\) and \(post\)).