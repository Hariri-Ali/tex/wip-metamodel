\section{The Authorisation Lifecycle}\label{sec:lifecycle}
We define the authorisation lifecycle as the series of phases through which an authorisation session passes during its lifetime from initial request to enforcement to revocation or termination.
The lifecycle of a continuous authorisation includes a temporal aspect as one or more phases span over a period of time.
In contrast, authorisations in traditional access control do not span over time, so their lifecycle phases are momentary and atomic (i.e., policy evaluation is performed only once).
The authorisation lifecycle depends on the use-case and the suitable access control model.
For instance, \ac{ucon} includes three phases only, such that the lifecycle starts with the \(pre\) phase then moves to the \(ongoing\) phase and ends with the \(post\) phase.
The authorisation lifecycle in emerging use-cases incorporates phases other than \ac{ucon}'s three phases.
For example, data flow lifecycle includes five phases: \(data\) \(collection\), \(data\) \(retention\), \(data\) \(access\), \(data\) \(transfer\) and \(data\) \(destruction\).
Other applications may require a lifecycle with phases for guiding users through the necessary steps to gain authorisation (e.g., \ac{mfa}, solving CAPTCHAs, improving security score).
In this section, we describe the properties of the authorisation lifecycle and show how it can be modelled as a \ac{dfa}.

\subsection{Lifecycle Phases}\label{sec:lcphases}
We define a phase as a particular step that characterises the authorisation at a particular point in its lifetime.
This, however, does not mean that a phase describes the context of the authorisation neither does it describe the subject or the object.
Thus, a phase must not be a decision factor in the security policy or the evaluation function (e.g, must not be an attribute in \ac{abac} or a role in \ac{rbac}).
It must only be a characteristic within the scope of the authorisation itself, describing the state or the progress of the authorisation.
For example, \ac{ucon}'s \(pre\) phase indicates that an access request has been made but access has not been granted yet, while the \(ongoing\) phase denotes that access has been granted and is in progress.
Another example is the \(retention\) phase in data flow, which indicates that data will be stored for a period of time as part of the data usage authorisation, thus data retention policies apply.
The series of phases and transitions between them constitute the authorisation lifecycle.
They portray the nature of the authorisation and define a scheme or a profile for enacting it.
The lifecycle determines how the authorisation should progress throughout its lifetime as different events occur and the evaluation decision changes.

\subsection{Classification of Decision Predicates}\label{sec:classification}
Since phases reflect the different stages in the authorisation, different rules apply at each phase.
Thus, decision predicates must be classified according to the phases of the authorisation lifecycle such that each class is evaluated only when the lifecycle enters the corresponding phase.
Such classification is a mapping from the set of decision predicates to the set of phases.
Policy evaluation may participate in driving the lifecycle such that the evaluation outcome in each phase determines the next phase in the lifecycle.
For instance, the lifecycle may specify that if the policy evaluation outcome of \textit{phaseA} is \(permit\), then the authorisation must move to \textit{phaseB}.
Transitions between phases may also be influenced by events other than the evaluation decision such as contextual events.

\subsection{Modelling the Lifecycle as a DFA}
The authorisation lifecycle consists of a set of phases and transitions between them.
For this reason, it can be modelled as a \ac{dfa} such that \ac{dfa} states represent phases, and the transition function together with the inputs represent the events that drive the lifecycle.
We use the same formal \ac{dfa} definition ~\cite{dfa} to define the lifecycle as the following 5-tuple \((Q,\Sigma,\delta,q_0,F)\)
such that:
\begin{itemize}
	\item \(Q\) is a finite set of phases;
	\item $\Sigma$ is a finite set of events and inputs (e.g., evaluation decisions, contextual events) used by the transition function to drive the lifecycle;
	\item \(\delta\) is a transition function \(\delta:Q\times\Sigma \rightarrow Q\);
	\item \(q_0\) is an initial phase;
	\item \(F\) is a finite set of terminal phases.
\end{itemize}

The authorisation lifecycle must have the same properties as a \ac{dfa}, so the following properties must be met when specifying a lifecycle in order to ensure its correctness:
\begin{itemize}
	\item The lifecycle must have one and only one initial phase.
	\item The lifecycle must have at least one exit or final phase.
	\item All phases must be reachable. 
	\item There must be one and only one transition for every unique input at each phase.
	\item The lifecycle must be deterministic, so the same events at a specific phase must always result in the same transition.
\end{itemize}
Examples of lifecycle \acp{dfa} are shown in Section ~\ref{sec:preliminary}.