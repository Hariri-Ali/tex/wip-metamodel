\acresetall

\section{Introduction}\label{sec:introduction}
With the rise of the ubiquitous Internet (e.g., smartphones, \ac{iot} and social networks), data security and user privacy have become major concerns fueled by privacy-related incidents such as the Cambridge Analytica scandal ~\cite{davies2015ted,punagin2015privacy}.
Access control has been traditionally used to protect data and privacy.
It evolved from coarse-grained models such as \ac{mac} and \ac{rbac} to more flexible and fine-grained models such as \ac{abac}.
However, these models assume that access rights do not change during access, so they do not react to situation changes.
For this reason, Park and Sandhu ~\cite{ucon} proposed the \ac{ucon} model as a generalisation that extends preceding models with attribute mutability and continuous control.
The novelties of \ac{ucon} enable it to revoke access when a change occurs and the security policy is no longer satisfied, which is required in long-lasting and dynamic authorisations such as cloud and \ac{iot} ~\cite{ucs2016cloudsys,ucs2017iot,ucs2017mqtt}.

\ac{ucon} introduced the concept of continuous authorisation that spans over a period of time \textit{within a session}, during which the security policy is re-evaluated and the authorisation decision is updated accordingly.
A continuous authorisation passes through different phases during its lifetime, and different rules apply at each phase.
We designate the set of such phases and the transitions between them as the \textit{authorisation lifecycle}.
\ac{ucon} employs a fixed authorisation lifecycle that consists of three phases only, so all \ac{ucon} authorisations always have the same lifecycle.

Governance initiatives and regulations, together with the accelerating growth of emerging technologies and use-cases have introduced more complicated authorisation requirements that cannot be met with \ac{ucon} for the following reasons:
\begin{enumerate}
	\item \ac{ucon} enacts continuity \textit{only while access is in progress}, while new technologies require continuous monitoring \textit{before} granting authorisation and/or \textit{after} revoking it.
	For instance, \acp{zta} rely on continuous and behavioural authentication as well as security scoring ~\cite{rose2020zero}, which requires monitoring before granting authorisation.
	Similarly, safety-critical applications such as smart vehicles and smart grids require monitoring after revoking authorisation to ensure that safety measures are taken ~\cite{humayed2017cyber}.
	\item \ac{ucon} \textit{revokes} and \textit{terminates} authorisations when the security policy is no longer satisfied.
	However, some use-cases need to \textit{temporarily suspend} the authorisation rather than completely revoking it.
	For example, changes in user behaviour or security score may not necessarily entail a termination of authorisation, but rather \textit{withholding} it to allow the user to improve their security score ~\cite{ucs2020plus}.
	\item \ac{ucon}'s lifecycle is static, coarse-grained and limited to three phases only, but new technologies and privacy laws necessitate finer-grained authorisation lifecycles based on different contexts or security threats.
	For example, big data lifecycle incorporates five different phases (i.e., data collection, retention, transfer, access and destruction) ~\cite{koo2020security, ruan2021privacy}.
\end{enumerate}

In this paper, we propose an extensible model for continuous authorisation and usage control, and lay out its building blocks as well as future research directions.
The model enables finer-grained, flexible and dynamic authorisation lifecycles by allowing its users to define any number and sequence of phases as required.
As such, the model can be instantiated into different flavours of concrete models, each specialised for a specific use-case, thus sustaining emerging applications as well as future use-cases.
The foundation of our model is modelling the authorisation lifecycle as a \ac{dfa} such that states represent phases.
The \ac{dfa} is then expressed in a structured language that can capture contextual information and define \ac{dfa} transitions accordingly.

The rest of this paper is organised as follows:
Section ~\ref{sec:background} describes background and related work; 
In Section ~\ref{sec:lifecycle}, we define the authorisation lifecycle and its phases, and show how it can be modelled as a \ac{dfa};
We introduce our model and explain how it enables user-defined lifecycles in Section ~\ref{sec:model};
We outline preliminary work and future directions in Section ~\ref{sec:preliminary}, and conclude in Section ~\ref{sec:conclusion}.