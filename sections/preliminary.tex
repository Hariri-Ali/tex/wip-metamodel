\section{Preliminary and Future Work}\label{sec:preliminary}
In this section, we describe our preliminary work to realise our model as well as example lifecycles.
We also lay out a roadmap of future actions to finalise the model and address open problems.

\subsection{Groundwork}
We introduced, in ~\cite{siuv}, an optimised implementation\footnote{Implementation of UCON model by Huawei German Research Centre} of \ac{ucon} using \ac{alfa} ~\cite{alfa} as a baseline policy language.
In this work, we modified our implementation to decouple the authorisation lifecycle from the authorisation process allowing the lifecycle to be configurable.
We added a component that evaluates lifecycle controllers and changes the phase accordingly.
\ac{alfa} rules incorporate condition and obligation expressions, which can be used to express if-else statements, so we used \ac{alfa} to express lifecycle controllers as shown in the example in Listing \ref{lst:lcrules}.
Complete examples are also hosted on GitLab\footnote{\label{fnt:gitlab}\url{https://gitlab.com/HaririAli/AuthorisationMetamodel}}.

\begin{lstlisting}[float=!tbh,caption={Example of lifecycle rule expressed in \ac{alfa}},label={lst:lcrules}]
	policy lifecycle {
		...
		rule ruleAtoB {
			target Attributes.session.phase == "phaseA"
			condition Attributes.session.decision == "permit"
			permit
			on permit {
				obligation obligationAtoB { Attributes.session.phase = "phaseB" }
		}}
		...
	};
\end{lstlisting}

\subsection{Example Lifecycles}
\subsubsection{UCON}
We constructed \ac{ucon} using our model by modelling its lifecycle as the \ac{dfa} shown in Figure ~\ref{fig:ucon}.
The lifecycle starts with the \(pre\) phase when access is requested.
If the evaluation outcome of \(pre\) rules is \(deny\), the authorisation ends.
Otherwise, the lifecycle moves to the \(ongoing\) phase and remains there as long as re-evaluations of \(ongoing\) rules result in \(permit\).
Once an \(ongoing\) re-evaluation results in \(deny\), the lifecycle moves to \(post\) where it enacts \(post\) rules and exits.
We expressed \ac{ucon}'s lifecycle using \ac{alfa}, which is hosted on GitLab $^{\ref{fnt:gitlab}}$, alongside other artefacts.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.99\linewidth]{figures/ucon.png}
	\caption{Lifecycle of a \ac{ucon} authorisation}
	\label{fig:ucon}
\end{figure}

\subsubsection{Extended UCON}\label{sec:extucon}
As aforementioned, emerging technologies require continuous control before granting and after revoking authorisation.
They may also need to temporarily suspend authorisations rather than completely revoking them.
Such requirements cannot be met with \ac{ucon} because it enacts continuity in the \(ongoing\) phase only.
For this reason, we introduce an extended version of \ac{ucon} by modifying its \ac{dfa} as shown in Figure ~\ref{fig:extucon} and constructing it using our metamodel.
The extended lifecycle does not terminate the authorisation process unless all directives (e.g., safety measures) are completed.
It also does not revoke access but rather puts it \textit{on hold} as shown in the \ac{dfa}, which solves the problem of re-evaluation counters introduced by ~\cite{ucs2020plus}.
The lifecycle can be also further extended to capture contextual information and add finer-grained transitions (e.g., move to \(on\)-\(hold\) only if \(deny\) was caused by trust-level changes; revoke access otherwise).
This demonstrates the extensibility of our model, and how it can enact different lifecycles to address different requirements.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.99\linewidth]{figures/extucon.png}
	\caption{Lifecycle of extended \ac{ucon} authorisation}
	\label{fig:extucon}
\end{figure}

\subsubsection{Data Flow Control}\label{sec:dataflow}
Data protection and data-centric authorisation is another example that can be covered by our model.
Regulations such as the \ac{gdpr} define a data lifecycle that consists of five phases, namely \(collection\), \(retention\), \(access\), \(transfer\) and \(destruction\), where different policies and rules apply at each phase 
For instance, \(retention\) policies may specify that data can be retained for up to three months or until the data subject withdraws consent.
Subsequently, the lifecycle must move to the \(destruction\) phase, where policies specify that the data must be deleted and derivative data must be anonymised.
We model the data-flow lifecycle as a \ac{dfa} shown in Figure ~\ref{fig:dataflow}, which can be expressed and enacted by our model.
The data-flow lifecycle may be more complicated than the provided \ac{dfa} example and it may require capturing contextual or other information.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.95\linewidth]{figures/dataflow.png}
	\caption{Lifecycle of a data flow authorisation}
	\label{fig:dataflow}
\end{figure}

\subsection{Future Directions}
To complete our work and further develop the model, we plan to conduct a formal analysis and formalise the model.
We also plan to carry out more thorough and extensive case studies of the above examples, and to explore other use-cases.
This allows us to identify corner cases that have not been addressed.
Finally, we aim to solve three open problems identified and described below.

Firstly, we defined the authorisation request in Section ~\ref{sec:authprocess} as an assignment of values to a subset of attributes designated as \textit{request attributes}.
Request attributes must not be arbitrary as they should reflect a complete and legitimate evaluation request.
However, we cannot predetermine what kind of attributes must be request attributes as this changes from one use-cases to another.
Therefore, the model must add a layer of abstraction that allows users to express what constitutes a legitimate request.

Secondly, our model assumes that \textit{all} attributes are \textit{mutable} including request attributes.
However, value changes in some request attributes may reflect a significant change in the request, which makes authorisation process incoherent as it would not relate consistently to its previous phases and states anymore.
An example of such changes is a value change in the \texttt{location} attribute when the \textit{subject is expected to be immobile}.
This obviously does not apply to all request attributes, as value changes in other request attributes do not necessarily indicate a significant change in the request context.
For example, a value in change in \texttt{user\_balance} would be acceptable as it does not reflect a significant change in the request.
We can argue that identifier attributes (e.g., subject ID, object ID) must always be fixed in all use-cases, but such argument cannot always be valid for other attributes.
Therefore, a thorough analysis and study is needed to devise a systematic method for specifying which request attributes must be fixed in a particular use-case.
%In the meantime, we assume that the authorisation request is complete and coherent, and request attributes that must be fixed for a particular use-case are predefined by the user of the model.
%Considering these assumptions, we only propose an approach to capture changes in fixed attributes and invalidate the authorisation process accordingly, thus mitigating the problem.
%This can be achieved by labelling fixed request attributes as ``fixed'', and using lifecycle controllers to capture value changes in these attributes, and to react by terminating the authorisation process.
%This does not fundamentally solve the problem, but rather mitigates it by relying on the enforcement entity to label fixed attributes.

Thirdly, we used \ac{alfa} to express lifecycle controllers as if-else statements.
However, \ac{alfa} was not designed for this purpose and its syntax and semantics do not fully support expressing \acp{dfa}.
For this reason, we plan to consider and study other languages such as Amazon States Language and Drools ~\cite{asl,drools}, or perhaps to devise new language for this purpose if needed.