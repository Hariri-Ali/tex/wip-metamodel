# WiP: Metamodel for Continuous Authorisation and Usage Control

## Authors
**Ali Hariri**, Amjad Ibrahim, Theo Dimitrakos, Bruno Crispo

## Venue
the 27th ACM on Symposium on Access Control Models and Technologies (SACMAT) 2022

## Abstract
Access control has been traditionally used to protect data and privacy.
Traditional access control models (e.g., ABAC, RBAC) cannot meet modern security requirements as technologies spread over heterogeneous and dynamic environments that need continuous monitoring.
Modern models such as Usage Control (UCON) introduced the concept of continuous authorisation that has a lifecycle consisting of a series of phases through which the authorisation passes during its lifetime.
However, such models assume a fixed lifecycle for all authorisations, so they cannot satisfy emerging technologies (e.g., smart vehicles, zero-trust, data flow), which require various and fine-grained lifecycles.
Researchers have extended existing models to meet such requirements, but all solutions remain restrictive, as they are specially tailored for specific use-cases.
In this paper, we propose an extensible model for continuous authorisations and usage control.
The model enables its users to customise and dynamically configure the authorisation lifecycle as required by the use-case.
This adds a layer of abstraction, forming a metamodel that can be instantiated into different flavours of continuous authorisation models, each addressing specific requirements.
We also show that the authorisation lifecycle can be modelled as Deterministic Finite Automaton (DFA) and expressed in a structured language used by an evaluation engine to dynamically enact and manage the lifecycle.
We layout the building blocks of the proposed metamodel and devise future research directions.

---
